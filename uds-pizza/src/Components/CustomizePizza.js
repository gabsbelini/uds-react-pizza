import React, { Component } from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import { connect } from 'react-redux';
import { selectAdditional } from '../Redux/actions';
import { removeAdditional } from '../Redux/actions';
import Checkbox from '@material-ui/core/Checkbox';
import { withStyles } from '@material-ui/core';
import styles from './ComponentStyles/CustomizePizzaStyles'
import api from '../Service/Api'

class CustomizePizza extends Component {
  state = {
    checkedA: false,
    checkedB: false,
    checkedF: false
  };

  handleChange = name => event => {
    console.log(event.target.value)
    event.target.checked
      ? this.props.selectAdditional(JSON.parse(event.target.value))
      : this.props.removeAdditional(JSON.parse(event.target.value).additionalType);
    this.setState({ [name]: event.target.checked });
  };
  placeOrder = () => {
    // api.createOrder(jsonData)
    this.props.history.push('/resumo')
  }

  render() {
    const { classes } = this.props
    return (
      <Paper className={classes.rootPaper}>
        <Typography 
          variant="headline"
          component="h2"
          className={classes.subHeading}> Selecione 0 ou mais adicionais!</Typography>
        <Grid container>
          <Grid item>
            <Checkbox
              checked={this.state.checkedA}
              onChange={this.handleChange('checkedA')}
              value='{"additionalType": "bacon", "price": 3, "addedTime": 0}'
            />
          </Grid>
          <Grid item style={{alignSelf: 'center'}}>
            <Typography>Deseja Adicionar Bacon por R$3.00?</Typography>
          </Grid>
          </Grid>
          <Grid container>
            <Grid item>
              <Checkbox
                checked={this.state.checkedB}
                onChange={this.handleChange('checkedB')}
                value='{"additionalType": "cebola", "price": 0, "addedTime": 0}'
              />
            </Grid>
            <Grid item style={{alignSelf: 'center'}}>
              <Typography>Deseja Adicionar cebola sem custo adicional?</Typography>
            </Grid>
          </Grid>

          <Grid container>
            <Grid item>
            <Checkbox
              checked={this.state.checkedC}
              onChange={this.handleChange('checkedC')}
              value='{"additionalType": "Borda Recheada", "price": 5, "addedTime": 5}'
            />
            </Grid>
            <Grid item style={{alignSelf: 'center'}}>
              <Typography> Deseja adicionar borda recheada por R$5.00 e +5min de tempo de preparo?</Typography>
            </Grid>

          </Grid>
          <Grid container justify="center">
            <Grid item style={{alignItems: 'center'}}>
              <Button 
                style={{width: '20em'}}  
                color="primary" 
                variant="contained" 
                onClick={this.placeOrder}
              >
                Finalizar!
              </Button>
            </Grid>
          </Grid>
      </Paper>
    );
  }
}
const mapDispatchToProps = dispatch => {
  return {
    selectAdditional: additional => dispatch(selectAdditional(additional)),
    removeAdditional: additional => dispatch(removeAdditional(additional))
  };
};

const mapStateToProps = state => {
  return { pizzaPrice: state.pizzaPrice };
};
export default withStyles(styles)(
  connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomizePizza)
);
