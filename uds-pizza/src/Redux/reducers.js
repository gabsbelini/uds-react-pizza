
const initialState = {
  flavor: {
    flavor: '',
    addedTime: 0,
  },
  size: {
    size: '',
    addedTime: 0,
    price: 0,
  },
  additional: {
    additionalList: [],
  }
};
const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case "SELECT_FLAVOR":
      return { ...state, flavor: action.payload };
    case "SELECT_SIZE":
      return { ...state, size: action.payload };
    case "SELECT_ADDITIONAL":
      return { 
        ...state, 
        additional: {
            additionalList: [...state.additional.additionalList, action.payload]
        } 
      }
    case "REMOVE_ADDITIONAL":
      return {
        ...state,
        additional: {
          additionalList: state.additional.additionalList.filter(item => item.additionalType !== action.payload)
        }
      }
    default:
      return state;
  }
};
export default rootReducer;