import React, { Component } from 'react';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import { connect } from 'react-redux'
import { selectSize } from "../Redux/actions";

class ChooseSize extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pizzaSize: ''
    };
  }

  handleChange = event => {
    this.props.selectSize(JSON.parse(event.target.value))
    this.setState({
      pizzaSize: event.target.value
    });
  };
  render() {
    return (
      <Paper style={{padding: 20,   marginLeft: 'auto', marginRight: 'auto'}}>
        <Typography
          variant="headline"
          component="h2"
          style={{ color: 'red', justifyContent: 'center' }}
        >
          Selecione o tamanho de sua pizza!
        </Typography>
        <FormControl>
          <FormLabel>Tamanho</FormLabel>
          <RadioGroup value={this.state.pizzaSize} onChange={this.handleChange}>
            <FormControlLabel
              value='{"size":"pequena", "addedTime": 15, "price": 20}'
              control={<Radio />}
              label="Pequena"
            />

            <FormControlLabel value='{"size":"media", "addedTime": 20, "price": 30}' control={<Radio />} label="Media" />

            <FormControlLabel
              value='{"size":"grande", "addedTime": 25, "price": 40}'
              control={<Radio />}
              label="Grande"
            />
          </RadioGroup>
        </FormControl>
      </Paper>
    );
  }
}
const mapDispatchToProps = dispatch => {
  return {
    selectSize: size => dispatch(selectSize(size))
  };
};

const mapStateToProps = state => {
  return { pizzaPrice: state.pizzaPrice };
};
export default connect(mapStateToProps, mapDispatchToProps)(ChooseSize);
