import {create} from 'apisauce'


const api = create({
  baseURL: 'http://localhost:3001/',
  headers: {
    'Accept': 'application/json', 
    'Content-Type': 'application/json'
  }
})

const createOrder = (data) => api.post(`/createOrder/`, data)
export default {
  createOrder
}