import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import ChooseFlavor from '../../Components/ChooseFlavor';
import ChooseSize from '../../Components/ChooseSize';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { styles } from './AssemblePizzaContainerStyle';

class AssemblePizzaContainer extends Component {
  onSubmit = () => {
    this.props.history.push('/montar/adicionais');
  };
  render() {
    const { classes } = this.props;
    return (
      <Grid
        container
        direction="column"
        spacing={8}
        className={classes.rootContainer}
      >
        <Grid item style={{ width: '70%' }}>
          <ChooseSize handleSubmitSize />
        </Grid>
        <Grid item style={{ width: '70%' }}>
          <ChooseFlavor handleSubmitFlavor />
        </Grid>
        {this.props.flavor.flavor === '' || this.props.size.size === '' ? (
          <Button disabled>Escolha sua pizza!</Button>
        ) : (
          <Button onClick={this.onSubmit} color="primary" variant="contained">
            Proximo Passo!
          </Button>
        )}
      </Grid>
    );
  }
}
const mapStateToProps = state => {
  return {
    flavor: state.flavor,
    size: state.size
  };
};

export default withStyles(styles)(
  connect(
    mapStateToProps,
    null
  )(AssemblePizzaContainer)
);
