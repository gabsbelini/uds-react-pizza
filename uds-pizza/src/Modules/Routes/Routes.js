import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import AssemblePizzaContainer from '../AssemblePizza/AssemblePizzaContainer';
import CustomizePizza from '../../Components/CustomizePizza'
import PizzaSummary from '../../Components/PizzaSummary'


class Routes extends Component {
  render() {
    return (
      <Switch>
        <Route exact path={'/montar/sabor-e-tamanho'} component={AssemblePizzaContainer} />
        <Route exact path={'/montar/adicionais'} component={CustomizePizza} />
        <Route exact path={'/resumo'} component={PizzaSummary} />
      </Switch>  
    );
  };
};

export default Routes;