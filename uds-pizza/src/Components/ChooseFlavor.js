import React, { Component } from 'react';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import { connect } from "react-redux";
import { selectFlavor } from "../Redux/actions";

class ChooseFlavor extends Component {
  constructor(props) {
    super(props)
    this.state = {
      pizzaFlavor: ''
    }
  }

  handleChange = (event) => {
    this.props.selectFlavor(JSON.parse(event.target.value))
    this.setState({
      pizzaFlavor: event.target.value
    })
  }
  render() {
    return (
      <Paper style={{padding: 20,  marginLeft: 'auto', marginRight: 'auto'}}>
        <Typography variant="headline" component="h2" style={{color: 'red', justifyContent: 'center'}}>
          Selecione o sabor de sua pizza!
        </Typography>
        <FormControl>
          <FormLabel >Sabor</FormLabel>
          <RadioGroup
            value={this.state.pizzaFlavor}
            onChange={this.handleChange}
          >
            <FormControlLabel value='{"flavor": "calabresa", "addedTime": 0}' control={<Radio />} label="Calabresa" />
            <FormControlLabel value='{"flavor": "marguerita", "addedTime": 0}' control={<Radio />} label="Marguerita" />
            <FormControlLabel value='{"flavor": "portuguesa", "addedTime": 5}' control={<Radio />} label="Portuguesa" />
          </RadioGroup>
        </FormControl>
      </Paper>
    );
  }
}
const mapDispatchToProps = dispatch => {
  return {
    selectFlavor: flavor => dispatch(selectFlavor(flavor))
  };
};

const mapStateToProps = state => {
  return { pizzaPrice: state.pizzaPrice };
};
export default connect(mapStateToProps, mapDispatchToProps)(ChooseFlavor);