export default theme => ({
  toolbar2: theme.mixins.toolbar,
  parentDiv: {
    padding: 10,
    paddingRight:40,
  },
  filtersDiv: {
    width: '100%',
    marginLeft: 10
  },
})