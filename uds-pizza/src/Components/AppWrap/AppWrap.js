import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';
import { Typography } from '@material-ui/core';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import styles from './AppWrapStyles';

class AppWrap extends Component {
  sumAdditionalItem = key => {
    console.log(this.props.additional);
    if (this.props.additional.additionalList.length !== 0) {
      const k = this.props.additional.additionalList
        .map(item => item[key])
        .reduce((prev, next) => prev + next);
      return k;
    } else {
      return 0;
    }
  };
  render() {
    const { classes } = this.props;
    return (
      <div>
        <AppBar>
          <Toolbar>
            <Grid container direction="column">
              <Grid item>
                <Typography color="secondary" variant="headline">
                  PREÇO TOTAL:{' '}
                  {this.props.size.price + this.sumAdditionalItem('price')}
                </Typography>
              </Grid>
              <Grid item>
                <Typography color="secondary" variant="headline">
                  TEMPO DE PREPARO:{' '}
                  {this.props.flavor.addedTime +
                    this.props.size.addedTime +
                    this.sumAdditionalItem('addedTime')}
                </Typography>
              </Grid>
            </Grid>
          </Toolbar>
        </AppBar>
        <div className={classes.toolbar2} />
          {this.props.children}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    flavor: state.flavor,
    size: state.size,
    additional: state.additional
  };
};

export default withStyles(styles)(
  connect(
    mapStateToProps,
    null
  )(AppWrap)
);
