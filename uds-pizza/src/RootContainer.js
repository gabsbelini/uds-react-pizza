import React from 'react';
import App from './App';
import { Provider } from 'react-redux';
import store from './Redux/store';
export default class RootContainer extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <App />
      </Provider>
    );
  }
}
