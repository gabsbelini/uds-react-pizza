import React, { Component } from 'react';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';
import { selectAdditional } from '../Redux/actions';
import { removeAdditional } from '../Redux/actions';
import { withStyles } from '@material-ui/core/styles';
import styles from './ComponentStyles/PizzaSummaryStyles';

class PizzaSummary extends Component {
  sumAdditionalItem = key => {
    if (this.props.additional.length !== 0) {
      const k = this.props.additional
        .map(item => item[key])
        .reduce((prev, next) => prev + next);
      return k;
    } else {
      return 0;
    }
  };
  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.rootPaper}>
        <Typography style={{ color: 'red' }} variant="h3">
          Resumo do pedido
        </Typography>
        <Grid container>
          <Grid item>
            <Typography style={{ color: '#ffa500' }} variant="subtitle2">
              Tamanho:
            </Typography>
          </Grid>
          <Grid item style={{ alignSelf: 'center' }}>
            <Typography>
              {this.props.size.size}, Valor: R$
              {this.props.size.price.toFixed(2)}, Tempo de preparo:{' '}
              {this.props.size.addedTime}
            </Typography>
          </Grid>
        </Grid>
        <Grid container>
          <Grid item>
            <Typography 
              style={{ color: '#ffa500' }} 
              variant="subtitle2">
              Sabor:
            </Typography>
          </Grid>
          <Grid item>
            <Typography>
              {this.props.flavor.flavor}{' '}
              {this.props.flavor.addedTime !== 0
                ? ', tempo adicional: ' +
                  this.props.flavor.addedTime +
                  ' minutos'
                : null}
            </Typography>
          </Grid>
        </Grid>
        {this.props.additional.map((item, index) => (
          <Grid container>
            <Grid item>
              <Typography style={{ color: '#ffa500' }} variant="subtitle2">
                Adicional {index + 1}:
              </Typography>
            </Grid>
            <Grid item>
              <Typography>
                {item.additionalType}
                {item.addedTime !== 0
                  ? ', tempo adicional ' + item.addedTime
                  : null}
                {item.price !== 0
                  ? ', valor adicional R$' + item.price.toFixed(2)
                  : null}
              </Typography>
            </Grid>
          </Grid>
        ))}
        <Grid container>
          <Grid item>
            <Typography style={{ color: '#ffa500' }} variant="subtitle2">
              Preço total:
            </Typography>
          </Grid>
          <Grid item>
            <Typography>
              R$
              {(
                this.props.size.price + this.sumAdditionalItem('price')
              ).toFixed(2)}
            </Typography>
          </Grid>
        </Grid>
        <Grid container>
          <Grid item>
            <Typography style={{ color: '#ffa500' }} variant="subtitle2">
              Tempo de preparo:
            </Typography>
          </Grid>
          <Grid item>
            <Typography>
              {this.props.size.addedTime +
                this.props.flavor.addedTime +
                this.sumAdditionalItem('addedTime')}{' '}
              minutos
            </Typography>
          </Grid>
        </Grid>
      </Paper>
    );
  }
}
const mapDispatchToProps = dispatch => {
  return {
    selectAdditional: additional => dispatch(selectAdditional(additional)),
    removeAdditional: additional => dispatch(removeAdditional(additional))
  };
};

const mapStateToProps = state => {
  return {
    flavor: state.flavor,
    size: state.size,
    additional: state.additional.additionalList
  };
};
export default withStyles(styles)(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(PizzaSummary)
);
