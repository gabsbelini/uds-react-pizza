
export const selectFlavor = flavor => ({type: "SELECT_FLAVOR", payload: flavor})
export const selectSize = size => ({type: "SELECT_SIZE", payload: size})
export const updatePrice = price => ({type: "UPDATE_PRICE", payload: price})
export const selectAdditional = additional => ({type: "SELECT_ADDITIONAL", payload: additional})
export const removeAdditional = additional => ({type: "REMOVE_ADDITIONAL", payload: additional})