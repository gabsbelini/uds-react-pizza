# UDS Pizzaria


Esse projeto foi criado como parte do processo seletivo da empresa UDS Tecnologia.
O projeto consiste na criação de uma aplicação em ReactJS onde o cliente pode montar seu pedido, selecionando o sabor e tamanho da pizza, bem como se deseja algum adicional extra.

# Executando o projeto

- Clone o respositório: `git clone https://gabsbelini@bitbucket.org/gabsbelini/uds-react-pizza.git`
- Instale as dependências: `npm install`
- Execute o projeto: `npm start`
