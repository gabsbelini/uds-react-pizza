import React, { Component } from 'react';
import { Router, Switch } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';
import { Provider } from 'react-redux';
import store from './Redux/store';
import Routes from './Modules/Routes/Routes';
import AppWrap from './Components/AppWrap/AppWrap';
import { MuiThemeProvider } from '@material-ui/core/styles';

const history = createHistory();

class App extends Component {
  render() {
    if (history.location.pathname !== '/montar/sabor-e-tamanho') {
      history.replace('/montar/sabor-e-tamanho');
    }
    return (
      <MuiThemeProvider>
        <Provider store={store}>
          <Router history={history}>
            <Switch>
              <AppWrap>
                <Routes />
              </AppWrap>
            </Switch>
          </Router>
        </Provider>
      </MuiThemeProvider>
    );
  }
}

export default App;
